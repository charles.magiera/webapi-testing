﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Academy_Tests.Utils;
using Snapshooter.NUnit;

//3 Add JsonBody generic (currently tightly coupled to Client)
//2 Data driven testing / One TestCase for 200 / One TestCse for 400
//4 ByPass Auth
//5 Rename setUrl method with endPoint
//6 Format response to a list snapshot

namespace Academy_Tests.PostClients
{
    public class PostClients
    {
        private const string getUrl = "https://localhost:5001/api/client";
        //Instantiate an instance of the constructor ClientHelper
        public PostClients()
        {
            this.clientHelper = new ClientHelper();
        }

        ClientHelper clientHelper { get; set; }

        [Test]
        public async Task PostClient_TitleMr_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mr", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_TitleMiss_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Miss", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_TitleDr_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Dr", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_TitleMrs_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mrs", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_TitleIncorret_Returns400()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Lord", "Charlotte", "Bronte", "emilybronte@gmail.com", "Work", "07999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result);
        }

            [Test]
        public async Task PostClient_PhoneTypeMobile_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mrs", "Emily", "Bronte", "emilybronte@gmail.com", "Mobile", "+447999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_PhoneTypeHome_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mrs", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_PhoneTypeWork_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mrs", "Emily", "Bronte", "emilybronte@gmail.com", "Work", "+447999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_PhoneTypeIncorret_Returns400()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Miss", "Charlotte", "Bronte", "emilybronte@gmail.com", "Space", "07999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result);
        }

        [Test]
        public async Task PostClient_PhoneValidPlus44_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mrs", "Emily", "Bronte", "emilybronte@gmail.com", "Work", "+447999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_PhoneValidZero_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mrs", "Emily", "Bronte", "emilybronte@gmail.com", "Work", "07999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_PhonePlus33Incorret_Returns400()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Miss", "Charlotte", "Bronte", "emilybronte@gmail.com", "Space", "+337999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.traceId"));
        }

        [Test]
        public async Task PostClient_DetailsAll_Returns200()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mrs", "Charlotte", "Bronte", "emilybronte@gmail.com", "Work", "07999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        [Test]
        public async Task PostClient_MissingTitle_Returns400()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("", "Charlotte", "Bronte", "emilybronte@gmail.com", "Work", "07999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result);

        }

        [Test]
        public async Task PostClient_MissingLastName_Returns400()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mr", "Charlotte", "", "emilybronte@gmail.com", "Work", "07999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result);

        }

        [Test]
        public async Task PostClient_MissingFirstName_Returns400()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mr", "", "Bronte", "emilybronte@gmail.com", "Work", "07999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result);

        }

        [Test]
        public async Task PostClient_MissingEmail_Returns400()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mr", "Charlotte", "Bronte", "", "Work", "07999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result);

        }

        [Test]
        public async Task PostClient_MissingPhone_Returns400()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mr", "Charlotte", "Bronte", "emilybronte@gmail.com", "Work", "");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result);

        }

        [Test]
        public async Task PostClient_MissingPhoneType_Returns400()
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody("Mr", "Charlotte", "Bronte", "emilybronte@gmail.com", "", "07999999999");
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result);

        }

    }
}
