﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Academy_Tests.Utils;
using Snapshooter.NUnit;

namespace Academy_Tests.PostClients_Chain
{
    public class PostClients_Chain
    {
        private const string getUrl = "https://localhost:5001/api/client";
        //Instantiate an instance of the constructor ClientHelper
        public PostClients_Chain()
        {
            this.clientHelper = new ClientHelper();
        }

        ClientHelper clientHelper { get; set; }


        //Valid TCs
        [TestCase("Mr", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999", TestName = "PostClient_TitleMr_Returns200")]
        [TestCase("Miss", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999", TestName = "PostClient_TitleMiss_Returns200")]
        [TestCase("Dr", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999", TestName = "PostClient_TitleDr_Returns200")]
        [TestCase("Mrs", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999", TestName = "PostClient_TitleMrs_Returns200")]
        [TestCase("Mr", "Emily", "Bronte", "emilybronte@gmail.com", "Mobile", "+447999999999", TestName = "PostClient_PhoneType_Mobile_Returns200")]
        [TestCase("Miss", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999", TestName = "PostClient_PhoneType_Home_Returns200")]
        [TestCase("Dr", "Emily", "Bronte", "emilybronte@gmail.com", "Work", "+447999999999", TestName = "PostClient_PhoneType_Work_Returns200")]
        [TestCase("Miss", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999", TestName = "PostClient_PhoneValid_Plus44_Returns200")]
        [TestCase("Dr", "Emily", "Bronte", "emilybronte@gmail.com", "Work", "07999999999", TestName = "PostClient_PhoneValid_07_Returns200")]
        public async Task PostClient_Receive200(string title, string firstName,
            string lastName, string emailAddress, string phoneType, string phoneNumber)
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody(title, firstName, lastName, emailAddress, phoneType, phoneNumber);
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }

        //Negative TCs
        [TestCase("Lord", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+447999999999", TestName = "PostClient_TitleIncorrrect_Returns400")]
        [TestCase("Miss", "Emily", "Bronte", "emilybronte@gmail.com", "Home", "+337999999999", TestName = "PostClient_Phone_Plus33Invalid_Returns400")]
        [TestCase("Dr", "Emily", "Bronte", "emilybronte@gmail.com", "Space", "+447999999999", TestName = "PostClient_PhoneTypeInvalid_Returns400")]
        [TestCase("", "Emily", "Bronte", "emilybronte@gmail.com", "Work", "+447999999999", TestName = "PostClient_MissingTitle_Returns400")]
        [TestCase("Dr", "", "Bronte", "emilybronte@gmail.com", "Work", "+447999999999", TestName = "PostClient_MissingFirstName_Returns400")]
        [TestCase("Dr", "Emily", "", "emilybronte@gmail.com", "Work", "+447999999999", TestName = "PostClient_MissingLastName_Returns400")]
        [TestCase("Dr", "Emily", "Bronte", "", "Work", "+447999999999", TestName = "PostClient_MissingEmail_Returns400")]
        [TestCase("Dr", "Emily", "Bronte", "emilybronte@gmail.com", "", "+447999999999", TestName = "PostClient_MissingPhoneType_Returns400")]
        [TestCase("Dr", "Emily", "Bronte", "emilybronte@gmail.com", "Work", "", TestName = "PostClient_MissingPhoneNumber_Returns400")]
        public async Task PostClient_Receive400(string title, string firstName,
            string lastName, string emailAddress, string phoneType, string phoneNumber)
        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Post);
            await this.clientHelper.AddClientJsonBody(title, firstName, lastName, emailAddress, phoneType, phoneNumber);
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            if (phoneNumber == "+337999999999")
            {
                Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.traceId"));
            }
            else
            {
                Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result);
            }
        }

    }
}
