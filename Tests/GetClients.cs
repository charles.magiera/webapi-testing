﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Academy_Tests.Utils;
using Snapshooter.NUnit;

namespace Academy_Tests.GetEndPoint
{
    public class GetClients
    {
        private string getUrl = "https://localhost:5001/api/client";
        //Instantiate an instance of the constructor ClientHelper
        public GetClients()
        {
            this.clientHelper = new ClientHelper();
        }

        ClientHelper clientHelper { get; set; }


        [Test]
        public async Task GetClients_Returns_200_ListOfClientsAsync()


        {
            this.clientHelper.SetUrl(getUrl);
            this.clientHelper.SetMethod(HttpMethod.Get);
            var response = await this.clientHelper.SendRequest();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Snapshot.Match(this.clientHelper.httpResponseMessage.Content.ReadAsStringAsync().Result, matchOptions => matchOptions.IgnoreField("**.id"));
        }
    }
}
