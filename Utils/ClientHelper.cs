﻿using System.Net.Http;
using System;
using System.Threading.Tasks;
using System.Text;
using Academy_Tests.Model;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Collections.Generic;
using Snapshooter.NUnit;
using System.Net.Http.Json;
using Academy_Tests.Utils;

namespace Academy_Tests.Utils
{
    public class ClientHelper
    {

        public ClientHelper()
        {

            this.httpRequestMessage = new HttpRequestMessage();
            this.httpClient = WebAppFactory.SetupClient();
            //this.BasicAuth("Steve.Jobs", "Password01!");
        }

        public HttpRequestMessage httpRequestMessage { get; set; }
        public HttpClient httpClient { get; set; }
        public TestAuthHandler TestAuthHandler { get; }
        public HttpResponseMessage httpResponseMessage { get; set; }

        

        public void BasicAuth(string username, string password)
        {
            string credentials = username + ":" + password;
            this.httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue(
                    "Basic", Convert.ToBase64String(
                      System.Text.ASCIIEncoding.ASCII.GetBytes(
                       $"{username}:{password}")));
        }

        public void SetUrl(string url)
        {
            this.httpRequestMessage.RequestUri = new Uri(url);
        }

        public void SetMethod(HttpMethod method)
        {
            this.httpRequestMessage.Method = method;
        }

        public async Task<HttpResponseMessage> SendRequest()
        {
            var request = httpRequestMessage.Clone();
           
            return this.httpResponseMessage = await this.httpClient.SendAsync(request);

        }


        //public string ConverterString()
        //{
        //    var responseData = this.httpResponseMessage.Content.ReadAsStringAsync();

        //    return responseData.Result;
        //}

        public async Task<HttpContent> AddClientJsonBody(string title, string firstName, 
            string lastName, string emailAddress, string phoneType, string phoneNumber)
        {
            var firstNewClient = new Client()
            {
                Id = Guid.NewGuid(),

                Title = title,

                FirstName = firstName,

                LastName = lastName,

                Email = emailAddress,

                ContactDetails = new ContactDetails()
                {
                    PhoneType = phoneType,

                    PhoneNumber = phoneNumber,
                }
            };

            var json = JsonConvert.SerializeObject(firstNewClient);
            var newClient = new StringContent(json, Encoding.UTF8, "application/json");
            
            return httpRequestMessage.Content = newClient;
        }
        
    }
}

