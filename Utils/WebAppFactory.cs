﻿using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
//using Academy_Tests.Utils.TestHandler;

namespace Academy_Tests.Utils
{
    public static class WebAppFactory
    {
        public static HttpClient SetupClient()
        {
            var webApplicationFactory = new WebApplicationFactory<WebAPI.Startup>()
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureServices(services =>
                    {
                        //services.AddAuthentication(options =>
                        //{
                        //    options.DefaultAuthenticateScheme = "Test";
                        //    options.DefaultChallengeScheme = "Test";
                        //    options.DefaultScheme = "Test";
                        //})
                        services.AddAuthentication("Test")
                            .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>("Test", null);

                    });
                });

            return webApplicationFactory.CreateClient();

        }

    }

}


