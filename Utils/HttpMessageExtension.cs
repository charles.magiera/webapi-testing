﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Academy_Tests.Utils
{
    public static class HttpMessageExtension
    {
        internal static HttpRequestMessage Clone(this HttpRequestMessage input)
        {
            var request = new HttpRequestMessage(input.Method, input.RequestUri)
            {
                Content = input.Content
            };
            foreach (var prop in input.Properties)
            {
                request.Properties.Add(prop);
            }
            foreach (var header in input.Headers)
            {
                request.Headers.TryAddWithoutValidation(header.Key, header.Value);
            }
            return request;
        }
    }
}
